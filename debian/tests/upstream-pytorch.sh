#!/bin/bash

set -e
set -u

testdir=tests
cp -R $testdir $AUTOPKGTEST_TMP
cd $AUTOPKGTEST_TMP

for py3ver in $(py3versions -vs)
do
    echo "Running tests with Python ${py3ver}."
    mkdir py-${py3ver}
    cp -R $testdir py-${py3ver}
    cd py-${py3ver}
    /usr/bin/python${py3ver} -B -m pytest -ra --backend pytorch $testdir
    cd ..
    echo "**********"
done
